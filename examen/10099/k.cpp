#include <iostream>
using namespace std;

#define NMAX 150

int main()
{
	cin.tie(0);
	ios::sync_with_stdio(0);

	int n,r,s,d,np,u,v,w,count,ans;
	int ma[NMAX][NMAX];

	count = 0;

	while(cin >> n >> r && n)
	{
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<n;j++)
				ma[i][j] = -1;

			ma[i][i] = 0;
		}

		for(int i=0;i<r;i++)
		{
			cin >> u >> v >> w;

			u--,v--;
			ma[u][v] = max(ma[u][v],w);
			ma[v][u] = ma[u][v];
		}

		cin >> s >> d >> np;

		for(int k=0;k<n;k++)
			for(int i=0;i<n;i++)
				for(int j=0;j<n;j++)
					if(ma[i][k] != -1 && ma[k][j] != -1)
						ma[i][j] = max(ma[i][j],min(ma[i][k],ma[k][j]));

		w = ma[--s][--d]-1;

		if(w == 0)
			ans = 1;
		else
			ans = np/w + (np%w != 0);

		cout << "Scenario #" << ++count << '\n';
		cout << "Minimum Number of Trips = " << ans << "\n\n";
	}

	return 0;
}
