#include <iostream>
#include <vector>
using namespace std;

typedef vector<int> vi;

vector<vi> G;
vi color;
int bipartite,black,white;

void dfs(int u)
{
	for(int i=0;i<G[u].size();i++)
	{
		int v = G[u][i];

		if(!color[v])
		{
			color[v] = -color[u];

			if(color[v] == 1) white++;
			else black++;

			dfs(v);
		}
		else
		{
			if(color[u] == color[v])
				bipartite = 0;
		}
	}
}

int main()
{
	cin.tie(0);
	ios::sync_with_stdio(0);

	int T,n,m,u,v,ans;

	cin >> T;

	for(int I=1;I<=T;I++)
	{
		cin >> n;

		G.clear();
		G.resize(n);
		color.assign(n,0);

		for(int i=0;i<n;i++)
		{
			cin >> m;

			u = i;

			for(int j=0;j<m;j++)
			{
				cin >> v;

				v--;

				if(v < n)
				{
					G[u].push_back(v);
					G[v].push_back(u);
				}
			}
		}

		ans = 0;

		for(int i=0;i<n;i++)
			if(!color[i])
			{
				bipartite = 1;

				black = white = 0;
				color[i] = ++white;

				dfs(i);

				if(bipartite)
					ans += max(black,white);
			}

		cout << ans << '\n';
	}

	return 0;
}
