#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

#define x first
#define y second

typedef pair<int,int> ii;
typedef pair<int,ii> iii;

vector<int> s;

void init(int n)
{
	if(s.size() < n)
		s.resize(n);

	for(int i=0;i<n;i++)
		s[i] = i;
}

int parent(int i)
{
	if(s[i] == i)
		return i;
	else
		return s[i] = parent(s[i]);
}

bool same_set(int i,int j)
{
	return parent(i) == parent(j);
}

void union_set(int i,int j)
{
	s[parent(i)] = parent(j);
}

int main()
{
	cin.tie(0);
	ios::sync_with_stdio(0);

	int t,n,m,a,x,y,c,ans,num_a;
	vector<iii> roads;

	cin >> t;

	for(int T=1;T<=t;T++)
	{
		cin >> n >> m >> a;

		if(roads.size() < m)
			roads.resize(m);

		init(n);
		ans = 0;
		num_a = n;

		for(int i=0;i<m;i++)
		{
			cin >> roads[i].y.x >> roads[i].y.y >> roads[i].x;

			roads[i].y.x--;
			roads[i].y.y--;
		}

		sort(roads.begin(),roads.begin()+m);

		for(int i=0;i<m;i++)
		{
			c = roads[i].x;
			x = roads[i].y.x;
			y = roads[i].y.y;

			if(a <= c)
				break;

			if(!same_set(x,y))
			{
				ans += c;
				union_set(x,y);
				num_a -= 1;
			}
		}

		ans += num_a * a;

		cout << "Case #" << T << ": " << ans << ' ' << num_a << '\n';
	}

	return 0;
}
