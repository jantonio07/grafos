def parent(arr,i):
	if arr[i] == i:
		return i
	else:
		arr[i] = parent(arr,arr[i])
		return arr[i]

def same_set(arr,i,j):
	return parent(arr,i) == parent(arr,j)

def union_set(arr,i,j):
	arr[parent(arr,i)] = parent(arr,j)

t = int(input())

for I in range(t):
	n,m,a = list(map(int,input().split()))

	roads = list([[0,0,0]] * m)
	ans = 0
	num_a = n
	s = [x for x in range(n)]

	for i in range(m):
		x,y,c = list(map(int,input().split()))

		x -= 1
		y -= 1

		roads[i] = [c,x,y]

	roads.sort()

	for road in roads:
		c = road[0]
		x = road[1]
		y = road[2]

		if a <= c:
			break
	
		if not same_set(s,x,y):
			ans += c
			union_set(s,x,y)
			num_a -= 1 
	
	ans += num_a * a

	print('Case #%d: %d %d' % (I+1,ans,num_a)) 
